Rails.application.routes.draw do
  get '/auth/:provider/callback', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'
  resources :sessions, only: [:new, :create]
  get 'profiles/show'
  root 'welcome#index'
  resources :users do
    resources :profiles, only: [:edit, :update, :show]
  end

  resources :coins do
    resources :comments
  end

  resources :bills do
    resources :comments
  end
end
