class BillsController < ApplicationController
  before_action :bill_resource,
                only: [:show, :edit, :update, :destroy]
  before_action :require_user, only: [:edit, :update, :destroy]

  def index
    @bills = Bill.all
  end

  def show
    @commentable = @bill
    @comments = @commentable.comments
    @comment = Comment.new
  end

  def new
    redirect_to root_path unless current_user
    @bill = Bill.new
  end

  def edit
  end

  def create
    @bill = current_user.bills.create(bill_params)
    if @bill.save
      redirect_to @bill
    else
      render :new
    end
  end

  def update
    if @bill.update(bill_params)
      redirect_to @bill
    else
      render :edit
    end
  end

  def destroy
    @bill.destroy
    redirect_to bills_path
  end

  private

  def bill_params
    params.require(:bill).permit(:nominal, :currency, :year,
                                 :country,
                                 :description,
                                 :count, :front_part_b,
                                 :rear_part_b)
  end

  def bill_resource
    @bill = Bill.find(params[:id])
  end

  def require_user
    redirect_to root_path unless current_user == @bill.user
  end
end
