class CoinsController < ApplicationController
  before_action :coin_resource,
                only: [:show, :edit, :update, :destroy]
  before_action :require_user_coins, only: [:edit, :update, :destroy]

  def index
    @coins = Coin.all
  end

  def show
    @commentable = @coin
    @comments = @commentable.comments
    @comment = Comment.new
  end

  def new
    redirect_to root_path unless current_user
    @coin = Coin.new
  end

  def edit
  end

  def create
    @coin = current_user.coins.create(coin_params)
    if @coin.save
      redirect_to @coin
    else
      render :new
    end
  end

  def update
    if @coin.update(coin_params)
      redirect_to @coin
    else
      render :edit
    end
  end

  def destroy
    @coin.destroy
    redirect_to coins_path
  end

  private

  def coin_params
    params.require(:coin).permit(:nominal, :currency, :year, :country,
                                 :metal, :description,
                                 :count, :front_part,
                                 :rear_part)
  end

  def coin_resource
    @coin = Coin.find(params[:id])
  end

  def require_user_coins
    redirect_to root_path unless current_user == @coin.user
  end
end
