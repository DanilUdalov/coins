class SessionsController < ApplicationController
  def new
  end

  def create
    @account = Account.find_by(account_params)
    @account = Account.create(account_params.merge(user_params)) unless @account
    session[:user_id] = @account.user_id
    redirect_to root_path
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: 'Logged out!'
  end

  private

  def auth_params
    request.env['omniauth.auth']
  end

  def account_params
    {
      uid: auth_params['uid'],
      provider: auth_params['provider']
    }
  end

  def user_params
    {
      user: User.find_or_create_by(email: auth_params['info']['email'])
    }
  end
end
