class Bill < ActiveRecord::Base
  belongs_to :user
  mount_uploader :front_part_b, FrontPartBUploader
  mount_uploader :rear_part_b, RearPartBUploader
  validates_presence_of :nominal
  validates_presence_of :currency
  validates_presence_of :year
  validates_presence_of :country
  validates_presence_of :count
  has_many :comments, as: :commentable

  def nominal_currency
    "#{nominal} #{currency}"
  end
end
