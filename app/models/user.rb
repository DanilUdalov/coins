class User < ActiveRecord::Base
  validates_uniqueness_of :email
  before_create :add_profile
  has_one :profile, dependent: :destroy
  has_many :accounts, dependent: :delete_all
  has_many :coins, dependent: :delete_all
  has_many :bills, dependent: :delete_all
  has_many :comments, dependent: :delete_all

  private

  def add_profile
    build_profile
  end
end
