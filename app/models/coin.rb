class Coin < ActiveRecord::Base
  belongs_to :user
  mount_uploader :front_part, FrontPartUploader
  mount_uploader :rear_part, RearPartUploader
  validates_presence_of :nominal
  validates_presence_of :currency
  validates_presence_of :year
  validates_presence_of :country
  validates_presence_of :count
  has_many :comments, as: :commentable

  def nominal_currency
    "#{nominal} #{currency}"
  end
end
