$(document).ready(function(){
    var preview_front_part = $(".upload-preview_front_part img");
    var preview_rear_part = $(".upload-preview_rear_part img");

    $(".file_front_part").change(function(event){
       var input = $(event.currentTarget);
       var file = input[0].files[0];
       var reader = new FileReader();
       reader.onload = function(e){
           image_base64 = e.target.result;
           preview_front_part.attr("src", image_base64);
       };
       reader.readAsDataURL(file);
    });
    $(".file_rear_part").change(function(event){
       var input = $(event.currentTarget);
       var file = input[0].files[0];
       var reader = new FileReader();
       reader.onload = function(e){
           image_base64 = e.target.result;
           preview_rear_part.attr("src", image_base64);
       };
       reader.readAsDataURL(file);
    });
});
