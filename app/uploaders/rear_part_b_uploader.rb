# encoding: utf-8

class RearPartBUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    [thumb, 'thumb_default.png'].compact.join('_')
  end

  version :thumb do
    process resize_to_fit: [200, 200]
  end

  version :medium do
    process resize_to_fit: [300, 300]
  end

  version :large do
    process resize_to_fit: [400, 400]
  end
end
