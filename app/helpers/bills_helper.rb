module BillsHelper
  def require_user_bills
    current_user == @bill.user
  end
end
