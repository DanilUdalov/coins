class AddUserToCoins < ActiveRecord::Migration
  def change
    add_reference :coins, :user, index: true, foreign_key: true
  end
end
