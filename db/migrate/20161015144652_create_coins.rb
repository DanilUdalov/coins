class CreateCoins < ActiveRecord::Migration
  def change
    create_table :coins do |t|
      t.integer :nominal
      t.string :currency
      t.string :year
      t.string :country
      t.string :metal
      t.text :description
      t.integer :count
      t.string :front_part
      t.string :rear_part

      t.timestamps null: false
    end
  end
end
