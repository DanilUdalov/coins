class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.integer :nominal
      t.string :currency
      t.string :year
      t.string :country
      t.text :description
      t.integer :count
      t.string :front_part_b
      t.string :rear_part_b
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
